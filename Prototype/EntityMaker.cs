﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prototype
{
    public class EntityMaker
    {
        private Entity prototype;

        public void SetPrototype(Entity prototype)
        {
            this.prototype = prototype;
        }

        public Entity Make()
        {
            return prototype.Clone() as Entity;
        }
    }
}
