﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prototype
{
    class Program
    {
        static void Main(string[] args)
        {
            var prototype1 = new Entity()
            {
                Id = Guid.NewGuid(),
                Name = "Misha",
                Age = 23
            };
            var prototype2 = new Entity()
            {
                Id = Guid.NewGuid(),
                Name = "Masha",
                Age = 24
            };

            var maker = new EntityMaker();
            var entities = new List<Entity>();

            maker.SetPrototype(prototype1);
            entities.Add(maker.Make());

            maker.SetPrototype(prototype2);
            entities.Add(maker.Make());
            entities.Add(maker.Make());

            foreach (var entity in entities)
                entity.Print();
        }
    }
}
