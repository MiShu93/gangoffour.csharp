﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prototype
{
    public class Entity : ICloneable
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }

        public void Print()
        {
            Console.WriteLine("Entity with name \"{0}\" have Id={1} and Age={2}.", Name, Id, Age);
        }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
