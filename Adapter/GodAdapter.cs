﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
    public class GodAdapter : IGod
    {
        private IMan man = new Man();

        public void MakeMagic()
        {
            man.MakeSomethingWonderful();
        }
    }
}
