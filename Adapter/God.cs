﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
    public interface IGod
    {
        void MakeMagic();
    }

    public class God : IGod
    {
        public void MakeMagic()
        {
            Console.WriteLine("Its magic!");
        }
    }
}
