﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
    public interface IMan
    {
        void MakeSomethingWonderful();
    }

    public class Man : IMan
    {
        public void MakeSomethingWonderful()
        {
            Console.WriteLine("Its magic too!");
        }
    }
}
