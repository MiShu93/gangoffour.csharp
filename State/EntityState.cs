﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State
{
    public enum EntityState
    {
        Stand,
        Steal,
        Jump
    }

    public class JumpState : IEntityState
    {
        private IEntity entity;

        public JumpState(IEntity entity)
        {
            this.entity = entity;
        }

        public void Down()
        {
            Console.WriteLine("Entity is descending!");
            entity.ChangeState(EntityState.Stand);
        }

        public void Up()
        {
            Console.WriteLine("Entity is jumping more higher!");
        }
    }

    public class StandState : IEntityState
    {
        private IEntity entity;

        public StandState(IEntity entity)
        {
            this.entity = entity;
        }

        public void Down()
        {
            Console.WriteLine("Entity is ducking!");
            entity.ChangeState(EntityState.Steal);
        }

        public void Up()
        {
            Console.WriteLine("Entity is jumping!");
            entity.ChangeState(EntityState.Jump);
        }
    }

    public class StealState : IEntityState
    {
        private IEntity entity;

        public StealState(IEntity entity)
        {
            this.entity = entity;
        }

        public void Down()
        {
            Console.WriteLine("Entity is still ducking!");
        }

        public void Up()
        {
            Console.WriteLine("Entity is rising!");
            entity.ChangeState(EntityState.Stand);
        }
    }
}
