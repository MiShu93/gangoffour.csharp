﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State
{
    public class Entity : IEntity
    {
        private IEntityState currentState;
        private Dictionary<EntityState, IEntityState> states = new Dictionary<EntityState, IEntityState>();

        public Entity()
        {
            states.Add(EntityState.Jump, new JumpState(this));
            states.Add(EntityState.Stand, new StandState(this));
            states.Add(EntityState.Steal, new StealState(this));

            currentState = states[EntityState.Stand];
        }

        public void ChangeState(EntityState entityState)
        {
            currentState = states[entityState];
        }

        public void Down()
        {
            currentState.Down();
        }

        public void Up()
        {
            currentState.Up();
        }
    }
}
