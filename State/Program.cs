﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State
{
    class Program
    {
        static void Main(string[] args)
        {
            var entity = new Entity();

            entity.Up();
            entity.Up();

            entity.Down();
            entity.Down();
            entity.Down();

            entity.Up();
        }
    }
}
