﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplateMethod
{
    public abstract class AbstractPrinter
    {
        protected string text;

        public void Print()
        {
            Read();
            Display();
        }

        protected abstract void Read();
        protected abstract void Display();
    }
}
