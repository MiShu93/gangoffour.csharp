﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplateMethod
{
    public class Printer : AbstractPrinter
    {
        protected override void Display()
        {
            Console.WriteLine(text);
        }

        protected override void Read()
        {
            text = File.ReadAllText("Text.txt");
        }
    }
}
