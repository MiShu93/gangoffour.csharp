﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge
{
    public class Dog
    {
        private DogImpl impl;

        public DogImpl Implementor
        {
            set { impl = value; }
        }

        public void Bark()
        {
            impl.Bark();
        }
    }
}