﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge
{
    class Program
    {
        static void Main(string[] args)
        {
            var dog = new Dog();

            dog.Implementor = new PugEnglish();
            dog.Bark();

            dog.Implementor = new PugRussian();
            dog.Bark();
        }
    }
}
