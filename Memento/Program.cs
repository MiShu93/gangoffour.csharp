﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memento
{
    class Program
    {
        static void Main(string[] args)
        {
            var originator = new Originator();
            var caretaker = new Caretaker(originator);

            originator.State = "First State";
            originator.Print();

            caretaker.KeepInMind();

            originator.State = "Second State";
            originator.Print();

            caretaker.CallToMind();

            originator.Print();
        }
    }
}
