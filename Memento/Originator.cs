﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memento
{
    public interface IOriginator
    {
        Memento CreateMement();
        void RestoreMemento(Memento memento);
    }

    public class Originator : IOriginator
    {
        private string state;

        public string State
        {
            set { state = value; }
        }

        public Memento CreateMement()
        {
            var memento = new Memento();
            memento.State = state;

            return memento;
        }

        public void RestoreMemento(Memento memento)
        {
            state = memento.State;
        }

        public void Print()
        {
            Console.WriteLine(state);
        }
    }
}
