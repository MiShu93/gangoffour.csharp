﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flyweight.Dogs
{
    public class Pug : DogInfo
    {
        public Pug()
        {
            breed = "Pug";
            height = 20;
            weight = 30;
        }
    }
}
