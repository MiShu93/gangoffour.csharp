﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flyweight.Dogs
{
    public class Dog
    {
        private string name;
        private DogInfo info;

        public Dog(string name, DogInfo info)
        {
            this.name = name;
            this.info = info;
        }

        public void Print()
        {
            Console.WriteLine("Dog with name \"{0}\":", name);
            info.Print();
        }
    }
}
