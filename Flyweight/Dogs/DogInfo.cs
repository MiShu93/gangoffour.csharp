﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flyweight.Dogs
{
    public abstract class DogInfo
    {
        protected int height;
        protected int weight;
        protected string breed;

        public void Print()
        {
            Console.WriteLine("breed = {0}", breed);
            Console.WriteLine("weight = {0}", weight);
            Console.WriteLine("height = {0}", height);
        }
    }
}
