﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flyweight.Dogs
{
    public class Dachshund : DogInfo
    {
        public Dachshund()
        {
            breed = "Dachshund";
            height = 15;
            weight = 25;
        }
    }
}
