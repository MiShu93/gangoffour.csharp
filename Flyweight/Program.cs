﻿using Flyweight.Dogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flyweight
{
    class Program
    {
        static void Main(string[] args)
        {
            var pugInfo = new Pug();
            var dachshundInfo = new Dachshund();

            var dogs = new List<Dog>();
            dogs.Add(new Dog("Bobik", pugInfo));
            dogs.Add(new Dog("Kolbaska", dachshundInfo));
            dogs.Add(new Dog("Sharik", pugInfo));

            foreach (var dog in dogs)
                dog.Print();
        }
    }
}
