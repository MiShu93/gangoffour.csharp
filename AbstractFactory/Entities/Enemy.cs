﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory.Entities
{
    public class Enemy : Entity
    {
        public Enemy(string name, EntityType type) : base(name, type) { }
    }
}
