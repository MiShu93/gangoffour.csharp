﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory.Entities
{
    public class Entity
    {
        public Entity() : this("Unnamed", EntityType.Unknown) { }

        public Entity(string name, EntityType type)
        {
            Name = name;
            Type = type;
        }

        public string Name { get; set; }
        public EntityType Type { get; set; }

        public void Print() => Console.WriteLine("Entity named \"{0}\" has type {1}.", Name, Type);

        public enum EntityType
        {
            Unknown,
            Player,
            Enemy
        }
    }
}
