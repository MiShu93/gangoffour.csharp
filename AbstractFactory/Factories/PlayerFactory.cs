﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbstractFactory.Entities;

namespace AbstractFactory.Factories
{
    public class PlayerFactory : IAbstractFactory
    {
        public Entity Create(string name)
        {
            return new Player(name, Entity.EntityType.Player);
        }
    }
}
