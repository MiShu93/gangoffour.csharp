﻿using AbstractFactory.Entities;
using AbstractFactory.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory
{
    class Program
    {
        static void Main(string[] args)
        {
            var entities = new List<Entity>();
            var factories = new List<IAbstractFactory>()
            {
                new PlayerFactory(),
                new EnemyFactory()
            };

            foreach (var factory in factories)
                entities.Add(factory.Create(Guid.NewGuid().ToString()));

            foreach (var entity in entities)
                entity.Print();
        }
    }
}
