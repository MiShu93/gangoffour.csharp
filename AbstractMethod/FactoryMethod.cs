﻿using AbstractMethod.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractMethod
{
    public class FactoryMethod
    {
        public void RegisterType<T>(Entity.EntityId id) where T : Entity
        {
            map.Add(id, typeof(T));
        }

        public Entity Create(Entity.EntityId id)
        {
            var type = map[id];
            return Activator.CreateInstance(type) as Entity;
        }

        private Dictionary<Entity.EntityId, Type> map = new Dictionary<Entity.EntityId, Type>();
    }
}