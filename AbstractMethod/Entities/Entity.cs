﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractMethod.Entities
{
    public class Entity
    {
        public Entity(string name)
        {
            Name = name;
        }

        public string Name { get; set; }

        public void Print() => Console.WriteLine("My name is \"{0}\".", Name);

        public enum EntityId
        {
            Unknown,
            Player,
            Enemy
        }
    }
}
