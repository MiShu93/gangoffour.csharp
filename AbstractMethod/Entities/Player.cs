﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractMethod.Entities
{
    public class Player : Entity
    {
        public Player() : base("Player") { }
    }
}
