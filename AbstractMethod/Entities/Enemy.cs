﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractMethod.Entities
{
    public class Enemy : Entity
    {
        public Enemy() : base("Enemy") { }
    }
}
