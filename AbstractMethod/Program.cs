﻿using AbstractMethod.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            var methods = new FactoryMethod();
            methods.RegisterType<Enemy>(Entity.EntityId.Enemy);
            methods.RegisterType<Player>(Entity.EntityId.Player);

            var entities = new List<Entity>();
            entities.Add(methods.Create(Entity.EntityId.Enemy));
            entities.Add(methods.Create(Entity.EntityId.Player));

            foreach (var entity in entities)
                entity.Print();
        }
    }
}
