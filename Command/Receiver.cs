﻿using Command.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command
{
    public class Receiver
    {
        private Document document = new Document();

        public void Open()
        {
            var command = new OpenCommand(document);
            command.Execute();
        }

        public void Close()
        {
            var command = new CloseCommand(document);
            command.Execute();
        }

        public void Print()
        {
            var command = new PrintCommand(document);
            command.Execute();
        }
    }
}
