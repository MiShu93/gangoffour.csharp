﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command.Commands
{
    public class OpenCommand : ICommand
    {
        private Document document;

        public OpenCommand(Document document)
        {
            this.document = document;
        }

        public void Execute()
        {
            document.Text = File.ReadAllText("Document.txt");
        }
    }
}
