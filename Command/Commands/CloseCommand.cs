﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command.Commands
{
    public class CloseCommand : ICommand
    {
        private Document document;

        public CloseCommand(Document document)
        {
            this.document = document;
        }

        public void Execute()
        {
            File.WriteAllText("Document.txt", document.Text);
            document.Text = "";
        }
    }
}
