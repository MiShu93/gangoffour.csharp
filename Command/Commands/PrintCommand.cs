﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command.Commands
{
    public class PrintCommand : ICommand
    {
        private Document document;

        public PrintCommand(Document document)
        {
            this.document = document;
        }

        public void Execute()
        {
            Console.WriteLine(document.Text);
        }
    }
}
