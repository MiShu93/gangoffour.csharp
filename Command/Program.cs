﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command
{
    class Program
    {
        static void Main(string[] args)
        {
            var documentReceiver = new Receiver();
            documentReceiver.Open();
            documentReceiver.Print();
            documentReceiver.Close();
        }
    }
}
