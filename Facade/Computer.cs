﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade
{
    public class Computer
    {
        private CPU cpu = new CPU();
        private Memory memory = new Memory();
        private HardDrive drive = new HardDrive();

        public void StartComputer()
        {
            cpu.Freeze();
            memory.Write(drive.ReadBootSector());
            cpu.Jump(memory.Read());
            cpu.Execute();
        }
    }
}
