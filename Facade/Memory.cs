﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade
{
    public class Memory
    {
        private int value;

        public void Write(int value)
        {
            this.value = value;
        }

        public int Read()
        {
            return value;
        }
    }
}
