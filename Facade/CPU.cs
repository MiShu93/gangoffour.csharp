﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade
{
    public class CPU
    {
        public void Freeze()
        {
            Console.WriteLine("CPU is freeezed.");
        }

        public void Jump(int address)
        {
            Console.WriteLine("Operation \"jump {0}\" is pushed to stack.", address);
        }

        public void Execute()
        {
            Console.WriteLine("All operations are executed!");
        }
    }
}
