﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy
{
    public class Poem : IPoem
    {
        private string text;

        public Poem()
        {
            text = File.ReadAllText("Poem.txt");
        }

        public int CountChar(char c)
        {
            return text.Count(x => x == c);
        }
    }
}
