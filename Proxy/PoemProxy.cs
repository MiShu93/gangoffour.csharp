﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy
{
    public class PoemProxy : IPoem
    {
        private Poem poem;

        public int CountChar(char c)
        {
            if (poem == null)
                poem = new Poem();

            return poem.CountChar(c);
        }
    }
}
