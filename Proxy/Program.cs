﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy
{
    class Program
    {
        static void Main(string[] args)
        {
            IPoem poem = new PoemProxy();
            int count = poem.CountChar('a');
            Console.WriteLine("Overlap 'a' count = {0}", count);
        }
    }
}
