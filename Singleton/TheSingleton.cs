﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singleton
{
    public class TheSingleton
    {
        private static TheSingleton instance = new TheSingleton();

        private TheSingleton() { }

        public static TheSingleton Instance() { return instance; }

        public void SayHello() => Console.WriteLine("Hello!");
    }
}
