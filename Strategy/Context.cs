﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    public class Context
    {
        public IStrategy Strategy { get; set; }
        
        public void DoStrategy()
        {
            Strategy.Say();
        }
    }
}
