﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    public interface IStrategy
    {
        void Say();
    }

    public class HelloStrategy : IStrategy
    {
        public void Say()
        {
            Console.WriteLine("Hello!");
        }
    }

    public class IntroduceStrategy : IStrategy
    {
        public void Say()
        {
            Console.WriteLine("My name is Misha.");
        }
    }

    public class GoodbyeStrategy : IStrategy
    {
        public void Say()
        {
            Console.WriteLine("Goodbye!");
        }
    }
}
