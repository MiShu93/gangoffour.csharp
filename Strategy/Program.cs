﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new Context();

            context.Strategy = new HelloStrategy();
            context.DoStrategy();

            context.Strategy = new IntroduceStrategy();
            context.DoStrategy();

            context.Strategy = new GoodbyeStrategy();
            context.DoStrategy();
        }
    }
}
