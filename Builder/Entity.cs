﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builder
{
    public class Entity
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public void Print() => Console.WriteLine("Entity with name \"{0}\" has {1} age.", Name, Age);

        public class Builder
        {
            private Entity entity = new Entity();

            public Builder SetName(string name)
            {
                entity.Name = name;
                return this;
            }

            public Builder SetAge(int age)
            {
                entity.Age = age;
                return this;
            }

            public Entity Build()
            {
                return entity;
            }
        }
    }
}
