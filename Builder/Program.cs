﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builder
{
    class Program
    {
        static void Main(string[] args)
        {
            var entity = new Entity.Builder()
                .SetName("Misha")
                .SetAge(23)
                .Build();

            entity.Print();
        }
    }
}
