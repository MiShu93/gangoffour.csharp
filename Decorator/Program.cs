﻿using Decorator.Components;
using Decorator.Decorators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator
{
    class Program
    {
        static void Main(string[] args)
        {
            var formula = new HalfDecorator(new PowDecorator(new NumberFour()));
            var solution = formula.Operate();
            Console.WriteLine("Answer is {0}.", solution);
        }
    }
}
