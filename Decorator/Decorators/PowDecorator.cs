﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Decorator.Components;

namespace Decorator.Decorators
{
    public class PowDecorator : Decorator
    {
        public PowDecorator(Component component) : base(component) { }

        public override int Operate()
        {
            var number = base.Operate();
            return number * number;
        }
    }
}
