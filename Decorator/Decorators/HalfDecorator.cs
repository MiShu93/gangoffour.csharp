﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Decorator.Components;

namespace Decorator.Decorators
{
    public class HalfDecorator : Decorator
    {
        public HalfDecorator(Component component) : base(component) { }

        public override int Operate()
        {
            return base.Operate() / 2;
        }
    }
}
