﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator.Components
{
    public class NumberFour : Component
    {
        public override int Operate()
        {
            return 4;
        }
    }
}
