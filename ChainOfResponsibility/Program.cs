﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility
{
    class Program
    {
        static void Main(string[] args)
        {
            ChainElement first = new First();
            ChainElement second = new Second();
            ChainElement third = new Third();

            first.Next = second;
            second.Next = third;

            Console.WriteLine("Say all:");
            first.Say(ChainCategory.All);

            Console.WriteLine("Only second:");
            first.Say(ChainCategory.Second);
        }
    }
}
