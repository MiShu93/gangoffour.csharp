﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility
{
    public class First : ChainElement
    {
        public First()
        {
            chainCategory = ChainCategory.First;
        }

        protected override void SaySomething()
        {
            Console.WriteLine("I'm First!");
        }
    }
}
