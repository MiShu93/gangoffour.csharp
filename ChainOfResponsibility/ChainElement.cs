﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility
{
    public abstract class ChainElement
    {
        protected ChainCategory chainCategory = ChainCategory.Default;
        private ChainElement nextElement = null;

        public ChainElement Next { set { nextElement = value; } }

        public void Say(ChainCategory category)
        {
            if (category == ChainCategory.All)
            {
                SaySomething();
                if (nextElement != null)
                    nextElement.Say(category);
            }
            else if (category == chainCategory)
            {
                SaySomething();
            }
            else if (nextElement != null)
            {
                nextElement.Say(category);
            }
        }

        protected abstract void SaySomething();
    }

    public enum ChainCategory
    {
        Default,
        All,
        First,
        Second,
        Third
    }
}
