﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility
{
    public class Third : ChainElement
    {
        public Third()
        {
            chainCategory = ChainCategory.Third;
        }

        protected override void SaySomething()
        {
            Console.WriteLine("I'm Third!");
        }
    }
}
