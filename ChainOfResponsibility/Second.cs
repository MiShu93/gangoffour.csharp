﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility
{
    public class Second : ChainElement
    {
        public Second()
        {
            chainCategory = ChainCategory.Second;
        }

        protected override void SaySomething()
        {
            Console.WriteLine("I'm Second!");
        }
    }
}
