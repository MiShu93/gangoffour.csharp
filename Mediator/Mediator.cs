﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator
{
    public interface IMediator
    {
        void Send(string message, string toName);
    }

    public class Mediator : IMediator
    {
        public ColleagueDeveloper Developer { get; set; }
        public ColleagueDesigner Designer { get; set; }

        public void Send(string message, string toName)
        {
            switch (toName)
            {
                case "Developer":
                    Developer.Notify(message);
                    break;
                case "Designer":
                    Designer.Notify(message);
                    break;
            }
        }
    }
}
