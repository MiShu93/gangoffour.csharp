﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator
{
    class Program
    {
        static void Main(string[] args)
        {
            var mediator = new Mediator();
            var developer = new ColleagueDeveloper(mediator);
            var designer = new ColleagueDesigner(mediator);

            mediator.Developer = developer;
            mediator.Designer = designer;

            developer.Send("Hi, man of colors", "Designer");
            designer.Send("Hi, man of bytes", "Developer");
        }
    }
}
