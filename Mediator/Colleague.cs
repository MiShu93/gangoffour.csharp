﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator
{
    public abstract class Colleague
    {
        private IMediator mediator;

        public Colleague(IMediator mediator)
        {
            this.mediator = mediator;
        }

        public void Send(string message, string toName)
        {
            mediator.Send(message, toName);
        }

        public abstract void Notify(string message);
    }

    public class ColleagueDeveloper : Colleague
    {
        public ColleagueDeveloper(IMediator mediator) : base(mediator)
        {
        }

        public override void Notify(string message)
        {
            Console.WriteLine("To Developer: {0}", message);
        }
    }

    public class ColleagueDesigner : Colleague
    {
        public ColleagueDesigner(IMediator mediator) : base(mediator)
        {
        }

        public override void Notify(string message)
        {
            Console.WriteLine("To Designer: {0}", message);
        }
    }
}
