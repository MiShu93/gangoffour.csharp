﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Visitor
{
    public class Couple : IMaxElement
    {
        public int A { get; set; }
        public int B { get; set; }

        public Couple(int a, int b)
        {
            A = a;
            B = b;
        }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
