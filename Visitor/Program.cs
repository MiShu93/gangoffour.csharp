﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Visitor
{
    class Program
    {
        static void Main(string[] args)
        {
            var visitor = new MaxVisitor();

            var couple = new Couple(10, 56);
            visitor.Visit(couple);
            Console.WriteLine(couple.Max);

            var triple = new Triple(9, 27, 3);
            visitor.Visit(triple);
            Console.WriteLine(triple.Max);
        }
    }
}
