﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Visitor
{
    public class MaxVisitor : IVisitor
    {
        public void Visit(Triple triple)
        {
            triple.Max = Math.Max(Math.Max(triple.A, triple.B), triple.C);
        }

        public void Visit(Couple couple)
        {
            couple.Max = Math.Max(couple.A, couple.B);
        }
    }
}
