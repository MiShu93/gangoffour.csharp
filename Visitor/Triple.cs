﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Visitor
{
    public class Triple : IMaxElement
    {
        public int A { get; set; }
        public int B { get; set; }
        public int C { get; set; }

        public Triple(int a, int b, int c)
        {
            A = a;
            B = b;
            C = c;
        }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
