﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iterator
{
    public interface Iterator<T>
    {
        void First();
        void Next();
        bool IsDone();
        T CurrentItem();
    }
}
