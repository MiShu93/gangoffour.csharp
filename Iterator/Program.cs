﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iterator
{
    class Program
    {
        static void Main(string[] args)
        {
            var list = new FixedArrayList<int>(3);

            list.Set(0, 10);
            list.Set(1, 666);
            list.Refresh(0);

            var iter = list.CreateIterator();
            for (iter.First(); !iter.IsDone(); iter.Next())
                Console.WriteLine(iter.CurrentItem());
        }
    }
}
