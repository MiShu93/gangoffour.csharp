﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iterator
{
    public interface IList<T>
    {
        Iterator<T> CreateIterator();
        int Count();
        T Get(int index);
        void Set(int index, T item);
        void Refresh(int index);
    }
}
