﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iterator
{
    public class FixedArrayListIterator<T> : Iterator<T>
    {
        private FixedArrayList<T> list;
        private int position;

        public FixedArrayListIterator(FixedArrayList<T> list)
        {
            this.list = list;
        }

        public T CurrentItem()
        {
            if (IsDone())
                throw new IndexOutOfRangeException();

            return list.Get(position);
        }

        public void First()
        {
            position = 0;
        }

        public bool IsDone()
        {
            if (position >= list.Count())
                return true;

            return false;
        }

        public void Next()
        {
            position++;
        }
    }
}
