﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iterator
{
    public class FixedArrayList<T> : IList<T>
    {
        private T[] array;

        public FixedArrayList(int capacity)
        {
            array = new T[capacity];
        }

        public void Set(int index, T item)
        {
            if (index < 0 || index >= Count())
                throw new IndexOutOfRangeException();

            array[index] = item;
        }

        public int Count()
        {
            return array.Count();
        }

        public Iterator<T> CreateIterator()
        {
            return new FixedArrayListIterator<T>(this);
        }

        public T Get(int index)
        {
            if (index < 0 || index >= Count())
                throw new IndexOutOfRangeException();

            return array[index];
        }

        public void Refresh(int index)
        {
            if (index < 0 || index >= Count())
                throw new IndexOutOfRangeException();

            array[index] = default(T);
        }
    }
}
