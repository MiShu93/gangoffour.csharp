﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer
{
    class Program
    {
        static void Main(string[] args)
        {
            var person = new Person();
            person.Name = "Person";
            person.Age = 0;

            var printer = new PrinterPersonObserver();
            person.Attach(printer);

            person.Name = "Misha";
            person.Age = 23;
        }
    }
}
