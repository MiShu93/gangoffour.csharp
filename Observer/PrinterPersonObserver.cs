﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer
{
    public class PrinterPersonObserver : IObserver
    {
        public void Update(Subject subject)
        {
            var person = subject as Person;

            Console.WriteLine("* Printer Observer *");
            Console.WriteLine(person.Name);
            Console.WriteLine(person.Age);
        }
    }
}
