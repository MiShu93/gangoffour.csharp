﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer
{
    public class Person : Subject
    {
        private string name;
        private int age;

        public string Name
        {
            set
            {
                name = value;
                Notify();
            }
            get { return name; }
        }

        public int Age
        {
            set
            {
                age = value;
                Notify();
            }
            get { return age; }
        }
    }
}
