﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite.Entities
{
    public class Entity
    {
        private Entity parent;
        private List<Entity> childrens;
        private string name;

        public Entity(string name)
        {
            this.name = name;
            this.childrens = new List<Entity>();
        }

        public void Add(Entity entity)
        {
            entity.parent = this;
            childrens.Add(entity);
        }

        public void Voice()
        {
            VoiceCurrent();
            VoiceChildrens();
        }

        protected virtual void VoiceCurrent() { }

        private void VoiceChildrens()
        {
            foreach (var child in childrens)
                child.Voice();
        }
    }
}
