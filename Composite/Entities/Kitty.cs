﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite.Entities
{
    public class Kitty : Entity
    {
        public Kitty(string name) : base(name) { }

        protected override void VoiceCurrent()
        {
            Console.WriteLine("Meow!");
        }
    }
}
