﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite.Entities
{
    public class Puppet : Entity
    {
        public Puppet(string name) : base(name) { }

        protected override void VoiceCurrent()
        {
            Console.WriteLine("Bark!");
        }
    }
}
