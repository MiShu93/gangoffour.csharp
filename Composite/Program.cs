﻿using Composite.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite
{
    class Program
    {
        static void Main(string[] args)
        {
            var root = new Entity("root");

            root.Add(new Puppet("Gaff"));
            root.Add(new Kitty("Miao"));

            root.Voice();
        }
    }
}
